navigator.serviceWorker.register('/sw.js');
  (function () {
    'use strict';
function doInstall() {
    console.log('doInstall');
    installButton.style.display = 'none';
    deferredPrompt.prompt();
    deferredPrompt.userChoice.then(res => {
        if (res.outcome === 'accepted') {
            console.log('doInstall: accepted');
        } else {
            console.log('doInstall: declined');
        }
         deferredPrompt = null;
    });
}


var urlParams = new URLSearchParams(window.location.search);
if (urlParams.get('source') === 'pwa') {
    console.log('Launched as PWA');
    let theTitle = document.getElementById('title');        
    theTitle.innerHTML = theTitle.innerHTML + ' (PWA)';
}

let installButton = document.getElementById('installButton');
installButton.onclick = doInstall;

let deferredPrompt;

window.addEventListener('beforeinstallprompt', event => {
    console.log('Event: beforeinstallprompt')
    event.preventDefault();
    deferredPrompt = event;
    installButton.style.display = 'block';
});

window.addEventListener('appinstalled', event => {
    console.log('App Installed');
});
  })();